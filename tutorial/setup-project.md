# Objectives

The objectives are:

- to deploy the ZipKin project and pipeline on the environment.

# Pre-requisite:

- [Setup the environment](./setup-environment.md)

# Tasks

1. Open Firefox on the development environment

2. Reach https://gitlab.mics.lu/users/sign_in and register a new user with the following parameters:
- Full ame: User
- Username: user
- Email: user@gitlab.mics.lu
- password: $YOUR_PASSWORD

3. Create a new project (https://gitlab.mics.lu/projects/new) named `lu.mics.devops.zipkin`

4. Open the terminal on the development environment and execute the following

```
git clone https://gitlab.com/mics-devops/lu.mics.devops.zipkin
cd lu.mics.devops.zipkin
git remote add gitlab https://gitlab.mics.lu/user/lu.mics.devops.zipkin.git
git push -u gitlab --all
```

Remark: You can also execute this on your own development environment or host. You might need to use `git -c http.sslVerify=false` instead of `git` for the commands.

# Validation

1. Open https://gitlab.mics.lu/user/lu.mics.devops.zipkin/pipelines in the browser.

The pipeline should be running.

2. After the pipeline execution the application should be running on:
 - http://staging.mics.lu:9411/
 - http://production.mics.lu:9411/
