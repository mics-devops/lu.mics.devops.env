# Objectives

The objectives are:

- to deploy a DevOps environment using virtualisation.

# Pre-requisite:

1. VirtualBox (v 6.0, or higher)<br>
- Instructions to install here: https://www.virtualbox.org/wiki/Downloads

2. Ansible
- Instructions to install here: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html.  

# Windows user note:
For windows eventhough WSL allows to link vagrant engine to the subsystem, it is highly recommended to run the `vagrant up` command in the windows environment (i.e. not through the WSL) as unpredictable behaviour were noticed when using vagrant inside WSL.\
\
The virtual machine provision with ansible works without issue as soon as the keys are correctly configured.

# Tasks

1. Generate a self signed key pair

```
cd certs
openssl req -x509 -newkey rsa:4096 -nodes -keyout devops-integration.key -out devops-integration.crt -days 365 -config devops-integration.conf
```

2. Generate ssh key pair pair

```
ssh-keygen -f ~/.ssh/id_rsa_devops_course
```
Leave passphrase empty (i.e. no password).

3. Add to the ~/.ssh/config file on host

```
Host devops-dev
    User vagrant
    Hostname 192.168.58.110
    IdentityFile ~/.ssh/id_rsa_devops_course

Host devops-integration
    User vagrant
    Hostname 192.168.58.111
    IdentityFile ~/.ssh/id_rsa_devops_course

Host devops-staging
    User vagrant
    Hostname 192.168.58.112
    IdentityFile ~/.ssh/id_rsa_devops_course

Host devops-production
    User vagrant
    Hostname 192.168.58.113
    IdentityFile ~/.ssh/id_rsa_devops_course
```

4. Add to the /etc/hosts file on host

```
192.168.58.111  gitlab.mics.lu
192.168.58.112  staging.mics.lu
192.168.58.113  production.mics.lu
```

5. Create the Integration virtual machine

```
cd env/integration
vagrant up
```

6. Provision the Integration virtual machine

```
cd ansible
ansible-playbook -i inventory playbook.yml
```

7. Repeat step 4 and 5 for the other environment (production, staging,
  development). Notice that staging and production environment depends on the integration environment.

8. Start the user interface on Development environment
- Start virtual box.
- Select development VM (right panel).
- Click show (top of left panel).
- Enter username and password (vagrant/vagrant)
- Enter the command `startx`

# Validation

- Reach https://gitlab.mics.lu from development environment
- Create the root password
- Log in root
- Reach https://gitlab.mics.lu/admin/runners

There should be 3 runners: 1 without tag, 1 with staging tag, 1 with production tag.
