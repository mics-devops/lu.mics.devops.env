This repository contains the Vagrant configuration and Ansible playbook to recreate the environment supporting the pipeline.

2 tutorials are provided:
1. [Setup environment](./tutorial/setup-environment.md) will help you to recreate the environment.
2. [Setup project](./tutorial/setup-project.md) will help you to install ZipKin and its pipeline in the environment.
